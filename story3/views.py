from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'home.html')

def photos(request):
    return render(request, 'gallery.html')