from django.contrib import admin
from .models import Form

# Register your models here.
class FormAdmin(admin.ModelAdmin):
	list_display = ['nama_mata_kuliah', 'dosen_pengajar', 'jumlah_sks', 'tahun_ajar',
			   'ruang_kelas']
	list_filter = ['tahun_ajar','jumlah_sks']
	# list_editable = ['nama']
	# prepopulated_fields = {'slug': ('title',)}

admin.site.register(Form, FormAdmin)