from django import forms
from .models import Form

class Input_Form(forms.ModelForm):
    class Meta:
        model = Form
        fields = ['nama_mata_kuliah', 'dosen_pengajar', 'jumlah_sks', 'deskripsi_mata_kuliah', 'tahun_ajar', 'ruang_kelas']
    
    error_messages = {
        'required' : 'Please Type'
    }
    nama_mata_kuliah = forms.CharField(widget=forms.TextInput(attrs={
        'maxlength' : 50,
        'type' : 'text',
        'required' : True
    }))
    dosen_pengajar = forms.CharField(widget=forms.TextInput(attrs={
        'maxlength' : 50,
        'type' : 'text',
        'required' : True
    }))
    jumlah_sks = forms.CharField(widget=forms.TextInput(attrs={
        'maxlength' : 2,
        'type' : 'text',
        'required' : True
    }))
    deskripsi_mata_kuliah = forms.CharField(widget=forms.Textarea(attrs={
        'maxlength' : 200,
        'rows' : 4,
        'type' : 'text',
        'required' : True
    }))
    tahun_ajar = forms.CharField(widget=forms.TextInput(attrs={
        'maxlength' : 20,
        'type' : 'text',
        'required' : True
    }))
    ruang_kelas = forms.CharField(widget=forms.TextInput(attrs={
        'maxlength' : 20,
        'type' : 'text',
        'required' : True
    }))
