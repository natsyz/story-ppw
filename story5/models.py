from django.db import models

# Create your models here.

class Form(models.Model):
    nama_mata_kuliah = models.CharField(max_length=50)
    dosen_pengajar = models.CharField(max_length=50)
    jumlah_sks = models.CharField(max_length=2)
    deskripsi_mata_kuliah = models.CharField(max_length=200)
    tahun_ajar = models.CharField(max_length=20)
    ruang_kelas = models.CharField(max_length=20)

    def __str__(self):
        return self.nama_mata_kuliah
