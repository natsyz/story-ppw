from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('isijadwal/', views.inputform, name='addform'),
    path('jadwal/', views.showform, name='showform'),
    path('saveform/', views.saveform, name='saveform'),
    path('jadwal/<id>/delete', views.deleteform ),
    path('jadwal/details/<id>', views.detailform ),
]
