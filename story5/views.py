from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from .formulir import Input_Form
from .models import Form

# Create your views here.
def inputform(request):
    response = {
        'input_form' : Input_Form,
    }
    return render(request, 'form.html', response)

def showform(request):
    response = {
        'form_table' : Form.objects.all(),
    }
    return render(request, 'jadwal.html', response)

def saveform(request):
    # form = Input_Form(request.POST or None)
    # return HttpResponseRedirect('/');
    form = Input_Form(request.POST or None)
    if (form.is_valid and request.method =='POST'):
        form.save()
        print("saved")
        return HttpResponseRedirect('/isijadwal')
    else:
        return HttpResponseRedirect('/isijadwal')

def deleteform(request, id): 
    obj = get_object_or_404(Form, id = id) 
    obj.delete() 
    return HttpResponseRedirect("/jadwal")

def detailform(request, id):
    response = {
        'item' : get_object_or_404(Form, id = id),
    }
    return render(request, 'fulldesc.html', response)