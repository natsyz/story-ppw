from django import forms
from .models import Acara, Peserta

class Form_Acara(forms.ModelForm):
    class Meta:
        model = Acara
        fields = ['nama_acara']
    
    error_messages = {
        'required' : 'Please Type'
    }
    nama_acara = forms.CharField(widget=forms.TextInput(attrs={
        'maxlength' : 75,
        'type' : 'text',
        'required' : True
    }))

class Form_Peserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama_peserta', 'nama_acara']
    
    error_messages = {
        'required' : 'Please Type'
    }
    nama_peserta = forms.CharField(widget=forms.TextInput(attrs={
        'maxlength' : 50,
        'type' : 'text',
        'required' : True
    }))