from django.db import models

# Create your models here.

class Acara(models.Model):
    nama_acara = models.CharField(max_length=75)

    def __str__(self):
        return self.nama_acara

class Peserta(models.Model):
    nama_peserta = models.CharField(max_length=50)
    nama_acara = models.ForeignKey(Acara, on_delete = models.CASCADE)

    def __str__(self):
        return self.nama_peserta
