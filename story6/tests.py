from django.test import TestCase, Client, RequestFactory
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import Acara, Peserta


# Create your tests here.
class TestAcara(TestCase):
    def test_story6_url_acara_ada(self):
        response = Client().get('/acara/')
        self.assertEqual(response.status_code, 200)

    def test_story6_template_acara_ada(self):
        response = Client().get('/acara/')
        self.assertTemplateUsed(response, 'acara.html')

    def test_story6_pakai_acara_func(self):
        found = resolve('/acara/')
        self.assertEqual(found.func, index)

    def test_story6_buat_model_acara(self):
        Acara.objects.create(nama_acara="PPW")
        hitungjumlah = Acara.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

    def test_story6_buat_model_peserta(self):
        acara = Acara.objects.create(nama_acara='Panen Boba')
        Peserta.objects.create(nama_peserta="PPW", nama_acara_id=acara.id)
        hitungjumlah = Peserta.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

    def test_print_model(self):
        acara = Acara.objects.create(nama_acara="Panen Boba")
        peserta = Peserta.objects.create(nama_peserta="Izuri", nama_acara=acara)
        self.assertEqual(str(acara), acara.nama_acara)
        self.assertEqual(str(peserta), peserta.nama_peserta)

    def test_story6_acara_bisa_simpan_POST_request(self):
        response = self.client.post('/acara/', data={'nama_acara': 'Panen Boba'})
        hitungjumlah = Acara.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/acara')

        new_response = self.client.get('/acara/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Panen Boba', html_response)

    def test_story6_peserta_bisa_simpan_POST_request(self):
        acara = Acara.objects.create(nama_acara='Panen Boba')
        nama = "Izuri"
        response = Client().post('/acara/', data={'nama_peserta' : nama, 'nama_acara' : acara.id})
        hitungjumlah = Peserta.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/acara')

        new_response = self.client.get('/acara/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Panen Boba', html_response)
        self.assertIn('Izuri', html_response)

    def test_story6_hubungan_model(self):
        Acara.objects.create(nama_acara='Panen Boba')
        idpk = Acara.objects.all()[0].id
        acara = Acara.objects.get(id=idpk)
        Peserta.objects.create(nama_peserta="Izuri", nama_acara=acara)
        jumlah = Peserta.objects.filter(nama_acara=acara).count()
        self.assertEqual(jumlah,1)

        new_response = self.client.get('/acara/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Panen Boba', html_response)
        self.assertIn('Izuri', html_response)

    def test_story6_pakai_deleteAcara_func(self):
        self.factory = RequestFactory()
        acara = Acara.objects.create(nama_acara='panen boba')
        alamat = 'acara/'+ str(acara.id) +'/deleteacara/'
        request = self.factory.get(alamat, {'id' : acara.id})
        response = deleteAcara(request, id=acara.id)
        self.assertEqual(response.status_code, 302)

    def test_story6_pakai_deletePeserta_func(self):
        self.factory = RequestFactory()
        acara = Acara.objects.create(nama_acara='panen boba')
        peserta = Peserta.objects.create(nama_peserta='Izuri', nama_acara=acara)
        alamat = 'acara/'+ str(peserta.id) +'/deletepeserta/'
        request = self.factory.get(alamat, {'id' : peserta.id})
        response = deletePeserta(request, id=peserta.id)
        self.assertEqual(response.status_code, 302)