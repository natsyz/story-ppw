from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('acara/', views.index, name='index'),
    path('acara/<id>/deleteacara', views.deleteAcara ),
    path('acara/<id>/deletepeserta', views.deletePeserta ),
]
