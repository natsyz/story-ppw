from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from .formulir import Form_Acara, Form_Peserta
from .models import Acara, Peserta

# Create your views here.
def index(request):
    if request.method == 'POST':
        if 'nama_peserta' in request.POST:
            form = Form_Peserta(request.POST or None)
            if form.is_valid():
                form.save()
            return HttpResponseRedirect('/acara')
        elif 'nama_acara' in request.POST:
            form = Form_Acara(request.POST or None)
            if form.is_valid():
                form.save()
            return HttpResponseRedirect('/acara')

    list_acara = Acara.objects.all()
    acara_detail = []

    for acara in list_acara:
        list_peserta = Peserta.objects.filter(nama_acara = acara)
        data_peserta = []
        for peserta in list_peserta:
            data_peserta.append(peserta)
        acara_detail.append((acara, data_peserta))

    response = {
        'form_peserta': Form_Peserta,
        'form_acara' : Form_Acara,
        'acara_detail' : acara_detail,
    }
    return render(request, 'acara.html', response)


def deleteAcara(request, id): 
    obj = get_object_or_404(Acara, id = id) 
    obj.delete() 
    return HttpResponseRedirect("/acara")

def deletePeserta(request, id):
    obj = get_object_or_404(Peserta, id = id) 
    obj.delete() 
    return HttpResponseRedirect("/acara")