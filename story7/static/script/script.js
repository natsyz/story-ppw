$(function() {
    let temp = '';
    let title = ['Who Am I?','Activities','Committee Experience','Achievement'];
    let content = [
        "<p> Hi I'm Nat. Nice to meet ya! I am an ordinary girl who tries her best on anything she does.</p><img src='https://i.ibb.co/1L3cVVn/barb.jpg' alt=''>",
        "<p class='hlight'>Academic</b><p>Learning from home, rushing deadline.</p><br><p class='hlight'>Game</p><p>Genshin Impact, Sudoku, Assassin's Creed</p><br><p class='hlight'>YouTube</p><p>Any interesting videos on my dashboard will do</p>",
        "<p class='hlight'>BKUI 20</b><p>Staff of Graphic Design</p><br><p class='hlight'>COMPFEST 12</p><p>Staff of Visual Design</p><br><p class='hlight'>PMB Fasilkom UI 2020</p><p>Staff of Academic</p><br><p class='hlight'>BETIS Fasilkom UI 2020</p><p>Staff of Mentor</p>",
        "<p>I guess making this website as byutipul as possible is already an achievement :D</p>"];

    $("#accordion").accordion({ header:".cover", collapsible: true, active: false, heightStyle: "content"});
    let i;
    for (i = 0; i < 4; i++) {
        $("#title-"+(i+1)).text(title[i]);
        $("#content-"+(i+1)).html(content[i]);
    };

    $(".cover:not(.btn-up, .btn-down)").click(function(){
        $( "#accordion" ).accordion( "enable" );
    });

    $(".btn-up").click(function(){
        $( "#accordion" ).accordion( "disable" );

        if ($(this).val() !== '1') {
            temp = title[$(this).val()-1];
            title[$(this).val()-1] = title[$(this).val()-2];
            title[$(this).val()-2] = temp;

            temp = content[$(this).val()-1];
            content[$(this).val()-1] = content[$(this).val()-2];
            content[$(this).val()-2] = temp;

            console.log(title);

            for (i = 0; i < 4; i++) {
                $("#title-"+(i+1)).text(title[i]);
                $("#content-"+(i+1)).html(content[i]);
            };
        }
    });

    $(".btn-down").click(function(){
        $( "#accordion" ).accordion( "disable" );
        
        if ($(this).val() !== '4') {
            temp = title[$(this).val()-1];
            title[$(this).val()-1] = title[$(this).val()];
            title[$(this).val()] = temp;

            temp = content[$(this).val()-1];
            content[$(this).val()-1] = content[$(this).val()];
            content[$(this).val()] = temp;

            console.log(title);

            for (i = 0; i < 4; i++) {
                $("#title-"+(i+1)).text(title[i]);
                $("#content-"+(i+1)).html(content[i]);
            };
        }
    });
})