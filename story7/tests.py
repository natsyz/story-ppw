from django.test import TestCase, Client
from django.urls import resolve
from .views import storytujuh

# Create your tests here.
class TestProfile(TestCase):
    def test_story7_url_profile_ada(self):
        response = Client().get('/story7')
        self.assertEqual(response.status_code, 200)

    def test_story7_template_profile_ada(self):
        response = Client().get('/story7')
        self.assertTemplateUsed(response, 'profile.html')

    def test_story7_pakai_profile_func(self):
        found = resolve('/story7')
        self.assertEqual(found.func, storytujuh)
