$(function() {
    $("#search-box").keyup(function(){
        let keyword = $("#search-box").val();
        $("#content").empty();
        if (!keyword){
            $.ajax({
                url: "data?q=sherlock%20holmes",
                success: function(result){
                    let array_items = result.items;
                    for (let i = 0; i < array_items.length; i+=2) {
                        let title1 = array_items[i].volumeInfo.title;
                        let image1 = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                        let title2 = array_items[i+1].volumeInfo.title;
                        let image2 = array_items[i+1].volumeInfo.imageLinks.smallThumbnail;
    
                        $("#content").append(
                            '<table><tr><td id="title">' + title1 + '</td><td id="title">' + title2 + '</td></tr><tr><td id="image-book"><img src=' + image1 + '></td><td id="image-book"><img src=' + image2 + '></td></tr></table>'
                        );
                    }
            }});
        } else {
            $.ajax({
                url: "data?q=" + keyword,
                success: function(result){
                    let array_items = result.items;
                    for (let i = 0; i < array_items.length; i+=2) {
                        let title1 = array_items[i].volumeInfo.title;
                        let image1 = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                        let title2 = array_items[i+1].volumeInfo.title;
                        let image2 = array_items[i+1].volumeInfo.imageLinks.smallThumbnail;
    
                        $("#content").append(
                            '<table><tr><td id="title">' + title1 + '</td><td id="title">' + title2 + '</td></tr><tr><td id="image-book"><img src=' + image1 + '></td><td id="image-book"><img src=' + image2 + '></td></tr></table>'
                        );
                    }
            }});
        }
    });

    if($("#search-box").is(':empty')){
        $.ajax({
            url: "data?q=sherlock%20holmes",
            success: function(result){
                let array_items = result.items;
                for (let i = 0; i < array_items.length; i+=2) {
                    let title1 = array_items[i].volumeInfo.title;
                    let image1 = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                    let title2 = array_items[i+1].volumeInfo.title;
                    let image2 = array_items[i+1].volumeInfo.imageLinks.smallThumbnail;

                    $("#content").append(
                        '<table><tr><td id="title">' + title1 + '</td><td id="title">' + title2 + '</td></tr><tr><td id="image-book"><img src=' + image1 + '></td><td id="image-book"><img src=' + image2 + '></td></tr></table>'
                    );
                }
        }});
    };
});