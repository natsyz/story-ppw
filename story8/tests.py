from django.test import TestCase, Client
from django.urls import resolve
from .views import story8_url, story8_data

# Create your tests here.
class TestProfile(TestCase):
    def test_story8_url_buku_ada(self):
        response = Client().get('/buku/')
        self.assertEqual(response.status_code, 200)

    def test_story8_template_buku_ada(self):
        response = Client().get('/buku/')
        self.assertTemplateUsed(response, 'buku.html')

    def test_story8_pakai_url_func(self):
        found = resolve('/buku/')
        self.assertEqual(found.func, story8_url)

    def test_story8_url_data_ada(self):
        response = Client().get('/buku/data?q=')
        self.assertEqual(response.status_code, 200)

    def test_story8_pakai_data_func(self):
        found = resolve('/buku/data')
        self.assertEqual(found.func, story8_data)
    
