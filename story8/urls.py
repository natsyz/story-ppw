from django.urls import path

from story8.views import story8_url, story8_data

app_name = 'story8'

urlpatterns = [
    path('', story8_url, name='index'),
    path('data', story8_data, name='data'),
]
