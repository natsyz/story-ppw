from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests #install dulu => pip install requests => update requirements.txt

# Create your views here.

def story8_url(request):
    response = {}
    return render(request, 'buku.html', response)

def story8_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url).json()
    return JsonResponse(ret, safe=False)