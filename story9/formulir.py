from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
# from .models import SignIn, SignUp

class SignUpForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
    
    error_messages = {
        'required' : 'Please Type'
    }
    password1 = forms.CharField(label=('Password'), widget=forms.PasswordInput(attrs={
        'minlength' : 8,
    }))
    def save(self, commit=True):
        print("masuk save formulir")
        user = super(SignUpForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            print("masuk save formulir commit")
            user.save()
        return user

# class SignInForm(forms.ModelForm):
#     class Meta:
#         model = User
#         fields = ['username', 'password']
    
#     error_messages = {
#         'required' : 'Please Type'
#     }
#     username = forms.CharField(widget=forms.TextInput(attrs={
#         'maxlength' : 75,
#         'type' : 'text',
#         'required' : True
#     }))
#     password = forms.CharField(widget=forms.PasswordInput(attrs={
#         'required' : True
#     }))