from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import resolve
from .views import signin_url, signup_url, logout_view
from .models import SignUp
from django.contrib.auth.forms import AuthenticationForm

# Create your tests here.
class TestProfile(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)

    def test_story9_url_signin_ada(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_story9_template_signin_ada(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'login.html')

    def test_story9_pakai_signin_func(self):
        found = resolve('/login')
        self.assertEqual(found.func, signin_url)

    def test_story9_signin_bisa_simpan_POST_request(self):
        response = self.client.post('/login', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)

        # self.assertEqual(response.status_code, 302)
        # self.assertEqual(response['location'], '/acara')

        # new_response = self.client.get('/acara/')
        # html_response = new_response.content.decode('utf8')
        # self.assertIn('Panen Boba', html_response)

    # def test_story9_buat_model_signin(self):
    #     SignIn.objects.create(username="user", password="passworduser")
    #     hitungjumlah = SignIn.objects.all().count()
    #     self.assertEqual(hitungjumlah, 1)

    def test_story9_url_signup_ada(self):
        response = Client().get('/signup')
        self.assertEqual(response.status_code, 200)

    def test_story9_pakai_signup_func(self):
        found = resolve('/signup')
        self.assertEqual(found.func, signup_url)

    def test_story9_template_signup_ada(self):
        response = Client().get('/signup')
        self.assertTemplateUsed(response, 'register.html')

    def test_story9_buat_model_signup(self):
        SignUp.objects.create(username="user", password="passworduser")
        hitungjumlah = SignUp.objects.all().count()
        self.assertEqual(hitungjumlah, 1)


    def test_story9_url_logout_ada(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code, 302)

    def test_story9_pakai_logout_func(self):
        found = resolve('/logout')
        self.assertEqual(found.func, logout_view)
    
