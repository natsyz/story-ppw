from django.urls import path

from story9.views import signin_url, signup_url, logout_view

app_name = 'story9'

urlpatterns = [
    path('login', signin_url, name='signin'),
    path('signup', signup_url, name='signup'),
    path('logout', logout_view, name='logout'),
]
