from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from .formulir import SignUpForm
from .models import SignUp
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import AuthenticationForm

# Create your views here.

def signup_url(request):
    errormsg = ''
    succeed = False
    username = ''

    if request.user.is_authenticated:
        return redirect('/')
    else:
        if request.method == 'POST':
            form = SignUpForm(request.POST)
            if form.is_valid():
                user = form.save()
                login(request, user)
                username = request.user.username
            else:
                succeed = False
                errormsg = form.errors
                print(form.errors)
        response = {
            'signUpForm': SignUpForm(),
            'errormsg' : errormsg,
            'succeed' : succeed,
            'username' : username,
        }
        return render(request, 'register.html', response)

def signin_url(request):
    errormsg = ''
    username = ''

    if request.user.is_authenticated:
        return redirect('/')
    else:
        if request.method == 'POST':
            form = AuthenticationForm(request, data=request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    username = request.user.username
                else:
                    errormsg = form.errors,
            else:
                errormsg = form.errors,
                print(form.errors)
        
        response = {
            'signInForm': AuthenticationForm(),
            'errormsg' : errormsg,
            'username' : username,
        }
        return render(request, 'login.html', response)

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')

